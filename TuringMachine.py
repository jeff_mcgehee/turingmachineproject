__author__ = 'Jeff'

blank_symbol = " "

class Tape(object):
    def __init__(self, input="",):
        self.tapeDict = {}
        for i in range(len(input)):
            self.tapeDict[i] = input[i]

    def __str__(self):
        s = ""
        min_used_index = min(self.tapeDict.keys())
        max_used_index = max(self.tapeDict.keys())+1
        for i in range(min_used_index,max_used_index):
            s += self.tapeDict[i]
        return s

    def __getitem__(self,index):
        if index in self.tapeDict:
            return self.tapeDict[index]
        else:
            return blank_symbol

    def __setitem__(self, pos, char):
        self.tapeDict[pos] = char


class TuringMachine(object):
    def __init__(self,
                 tape = "",
                 blank_symbol = " ",
                 tape_alphabet = ["0", "1"],
                 initial_state = "",
                 accepting_states = [],
                 final_states = [],
                 transition_function = {}):
        self.__tape = Tape(tape)
        self.head_position = 0
        self.__blank_symbol = blank_symbol
        self.current_state = initial_state
        self.__transition_function = transition_function
        self.__tape_alphabet = tape_alphabet
        self.__final_states = final_states

    def show_tape(self):
        print(self.__tape)
        return True#str(self.__tape)

    def step(self):
        char_under_head = self.__tape[self.head_position]
        x = (self.current_state, char_under_head)
        if x in self.__transition_function:
            y = self.__transition_function[x]
            self.__tape[self.head_position] = y[1]
            if y[2] == "R":
                 self.head_position += 1
            elif y[2] == "L":
                self.head_position -= 1
            self.current_state = y[0]

    def printConfig(self):
        s = ""
        for key in self.__tape.tapeDict.keys():
            if key == self.head_position:
                s+=self.current_state+self.__tape.tapeDict[key]
            else:
                s+=self.__tape.tapeDict[key]

        print(s)

    def final(self):
        if self.current_state in self.__final_states:
            return True
        else:
            return False