__author__ = 'Jeff'
from TuringMachine import TuringMachine

# initial_state = "init",
# accepting_states = ["final"],
transition_function = {("q0","0"):("q1", "X", "R"),
                       ("q0","1"):("q2", "X", "R"),
                       ("q0","#"):("q7", "#", "R"),
                       ("q1","0"):("q1", "0", "R"),
                       ("q1","1"):("q1", "1", "R"),
                       ("q1","#"):("q3", "#", "R"),
                       ("q2","0"):("q2", "0", "R"),
                       ("q2","1"):("q2", "1", "R"),
                       ("q2","#"):("q4", "#", "R"),
                       ("q3","X"):("q3", "X", "R"),
                       ("q3"," "):("qr", " ", "R"),
                       ("q3","1"):("qr", "1", "R"),
                       ("q3","0"):("q5", "X", "L"),
                       ("q4","X"):("q4", "X", "R"),
                       ("q4"," "):("qr", " ", "R"),
                       ("q4","0"):("qr", "1", "R"),
                       ("q4","1"):("q5", "X", "L"),
                       ("q5","X"):("q5", "X", "L"),
                       ("q5","#"):("q6", "#", "L"),
                       ("q6","0"):("q6", "0", "L"),
                       ("q6","1"):("q6", "1", "L"),
                       ("q6","X"):("q0", "X", "R"),
                       ("q7"," "):("qa", " ", "L"),
                       ("q7","0"):("qr", "0", "R"),
                       ("q7","1"):("qr", "1", "R"),
                       ("q7","#"):("qr", "#", "R"),
                       ("q7","X"):("q7", "X", "R"),
                       ("qr","0"):("qr", "0", "N"),
                       ("qr","1"):("qr", "1", "N"),
                       ("qr","#"):("qr", "#", "N"),
                       ("qa","0"):("qr", "0", "N"),
                       ("qa","1"):("qr", "1", "N"),
                       ("qa","#"):("qr", "#", "N"),
                       }
final_states = ["qa","qr"]

t = TuringMachine("01#01",
                  initial_state = "q0",
                  final_states = final_states,
                  transition_function=transition_function,
                  tape_alphabet=['0', '1', '#', 'X',' '])

print("Input on Tape:")
t.show_tape()

iter = 0
while not t.final():
    iter+=1
    print('\nConfiguration %d'%iter)
    t.step()
    t.printConfig()
    # print('Tape:')
    # t.show_tape()
    # print('Head Position:'+str(t.head_position))
    # print('State:'+t.current_state)
    # raw_input('press enter')

print("Result of the Turing machine calculation:")
t.show_tape()